package edu.ntnu.jonaskos.Sim;

import edu.ntnu.jonaskos.Units.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Terrain modifier test.
 */
class TerrainModifierTest {

    Unit ranger;

    Unit infantry;

    /**
     * Sets up the tests, this code runs before each test.
     */
    @BeforeEach
    void setUp() {
        infantry = new InfantryUnit("infantry", 100);
        ranger = new RangedUnit("ranger", 100);
    }


    /**
     * tests if setting the terrain works as expected.
     */
    @Test
    void getAndSetTerrain() {
        TerrainModifier.setTerrain("Forest");

        assertEquals(3, TerrainModifier.getTerrain());
    }

    /**
     * Tests if the correct terrain type is returned.
     */
    @Test
    void getTerrainTypes() {
        String[] terrainTypes = TerrainModifier.getTerrainTypes();

        assertEquals(3, terrainTypes.length);
    }

    /**
     * Tests if attack bonus is applied correctly.
     */
    @Test
    void attackBonusTest(){
        TerrainModifier.setTerrain("HILL");

        assertEquals(5, ranger.getAttackBonus());
    }

    /**
     * Tests if defense bonus is applied correctly.
     */
    @Test
    void resistBonusTest(){
        TerrainModifier.setTerrain("FOREST");

        assertEquals(4, infantry.getResistBonus());
    }
}