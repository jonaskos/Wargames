package edu.ntnu.jonaskos.Sim;

import edu.ntnu.jonaskos.Units.CavalryUnit;
import edu.ntnu.jonaskos.Units.CommanderUnit;
import edu.ntnu.jonaskos.Units.InfantryUnit;
import edu.ntnu.jonaskos.Units.Unit;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Army test.
 */
class ArmyTest {

    Army army1 = new Army("army1");
    CavalryUnit cavalry = new CavalryUnit("cavalry", 100);

    InfantryUnit infantry = new InfantryUnit("infantry", 100);

    InfantryUnit infantry2 = new InfantryUnit("infantry2",100);

    CommanderUnit commander = new CommanderUnit("commander",100);

    /**
     * Tests if getname returns the correct name.
     */
    @Test
    void getNameTest() {
        assertEquals("army1", army1.getName());
    }

    /**
     * Tests if addUnit adds a unit to the army.
     */
    @Test
    void addUnitTest() {
        army1.addUnit(cavalry);
        army1.addUnit(infantry);

        assertTrue(army1.hasUnits());
    }

    /**
     * Tests if addallunits adds all units to the army.
     */
    @Test
    void addAllUnitsTest() {
        ArrayList<Unit> units = new ArrayList<>();
        units.add(cavalry);
        units.add(infantry);

        army1.addAllUnits(units);

        assertTrue(army1.hasUnits());
    }

    /**
     * Tests if removeUnit removes a unit from the army.
     */
    @Test
    void removeUnit() {
        army1.addUnit(cavalry);
        army1.addUnit(infantry);

        army1.removeUnit(infantry);

        assertEquals(1, army1.getAllUnits().size());
    }

    /**
     * Tests if hasUnits returns true if the army has units.
     */
    @Test
    void hasUnits() {
        army1.addUnit(cavalry);

        assertTrue(army1.hasUnits());
    }

    /**
     * Tests if hasUnits returns false if the army has no units.
     */
    @Test
    void doesntHaveUnits() {
        assertFalse(army1.hasUnits());
    }
    /**
     * Tests if getAllUnits returns all units in the army.
     */
    @Test
    void getAllUnits() {
        army1.addUnit(cavalry);
        army1.addUnit(infantry);
        army1.addUnit(infantry2);
        army1.addUnit(commander);

        assertEquals(4, army1.getAllUnits().size());
    }

    /**
     * Tests if getRandomUnit returns a random unit from the army.
     */
    @Test
    void getRandomUnit() {
        army1.addUnit(cavalry);
        army1.addUnit(infantry);

        Unit unitPicked = army1.getRandomUnit();

        assertTrue(unitPicked.getName().equals("cavalry") || unitPicked.getName().equals("infantry"));
    }

    /**
     * Tests if get method for getting all units of a spesific type returns the correct units.
     */
    @Test
    void returnUnitTest(){
        army1.addUnit(infantry);
        army1.addUnit(cavalry);
        army1.addUnit(commander);
        army1.addUnit(infantry2);

        List<Unit> units = army1.getInfantryUnits();

        assertEquals(2, units.size());
    }


    /**
     * Test to see if returncavalry method also returns commander units since commander unit through inheritance is a type of cavalry unit.
     */
    @Test
    void returnCavalryReturnsOnlyCavalry(){
        army1.addUnit(cavalry);
        army1.addUnit(commander);

        List<Unit> units = army1.getCavalryUnits();
        System.out.println(units);
        assertEquals(1, units.size());
    }
}