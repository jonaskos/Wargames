package edu.ntnu.jonaskos.Sim;

import edu.ntnu.jonaskos.Units.CavalryUnit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Battle test.
 */
class BattleTest {

    Army humanArmy;

    Army orcishArmy;
    Battle battle;

    /**
     * Sets up the tests, this code runs before each test.
     */
    @BeforeEach
    void setup(){
        TerrainModifier.setTerrain("Forest");
        humanArmy = new Army("Humans");
        orcishArmy = new Army("Orcs");

        humanArmy.addUnit(new CavalryUnit("cavalry", 100));
        orcishArmy.addUnit(new CavalryUnit("cavalry", 100));
        battle = new Battle(humanArmy, orcishArmy);
    }

    /**
     * Tests the simulation
     * winner of the battle is expected to have units left while the loser should not.
     */
    @Test
    void SimulateTestWinner(){
        Army winner = battle.simulate();

        assertTrue(winner.hasUnits());
    }

    /**
     * Tests if the loser of the battle has no units left.
     */
    @Test
    void SimulateTestLooserHasNoUnits(){
        Army winner = battle.simulate();
        boolean hasUnits;
        if(winner.equals(humanArmy)){
            hasUnits = orcishArmy.hasUnits();
        } else hasUnits = humanArmy.hasUnits();

        assertFalse(hasUnits);
    }

    /**
     * Tests if simulate attack method sets the correct winner.
     */
    @Test
    void SimulateAttackTest(){
        while(battle.getWinner() == null){
            battle.simulateAttack();
        }

        Army winner = battle.getWinner();

        assertTrue(winner.hasUnits());
    }



}