package edu.ntnu.jonaskos;

import edu.ntnu.jonaskos.Sim.TerrainModifier;
import edu.ntnu.jonaskos.Units.CavalryUnit;
import edu.ntnu.jonaskos.Units.InfantryUnit;
import edu.ntnu.jonaskos.Units.RangedUnit;
import edu.ntnu.jonaskos.Units.Unit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class UnitTest {

    @BeforeEach
    void setUp() {
        TerrainModifier.setTerrain("FOREST");
    }
    /**
     * tests the Attack method. Units are of the same kind and the unit attacking first is expected to win.
     * The units take turns attacking until one of them has a health value below zero.
     */
    @Test
    void testAttackWhenSameUnit() {
        Unit infantry1 = new InfantryUnit("infantry1", 50);
        Unit infantry2 = new InfantryUnit("infantry2", 50);

        while (infantry1.getHealth() > 0 && infantry2.getHealth() > 0) {
            infantry1.attack(infantry2);
            if (infantry1.getHealth() < 0 || infantry2.getHealth() < 0)
                break;
            infantry2.attack(infantry1);
        }
        assertTrue(infantry1.getHealth() > infantry2.getHealth());


    }

    /**
     * tests if attack bonus is lowered after the initial attack for units that has this behavior.
     * Initial attack bonus is set to 6 and will be lowered to 2 after getAttackBonus is called on the unit.
     */
    @Test
    void testAttackBonusIsLowered() {
        Unit cavalry1 = new CavalryUnit("cavalry1", 50);

        for (int i = 0; i < 10; i++) {
            if (i == 0) {
                assertEquals(6, cavalry1.getAttackBonus());
            } else
                assertEquals(2, cavalry1.getAttackBonus());
        }
    }
    /**
     * tests if resist bonus is lowered after the initial attack for units that has this behavior.
     * Initial resist bonus is set to 6 and will be lowered to 4 after getAttackBonus is called on the unit. It will then get lowered to 2 and stay at this value.
     */
    @Test
    void testResistBonusIsLowered() {
        Unit ranged1 = new RangedUnit("ranged1", 50);

        for (int i = 0; i < 10; i++) {
            if (i == 0) {
                assertEquals(6, ranged1.getResistBonus());
            } else if (i == 1) {
                assertEquals(4, ranged1.getResistBonus());
            } else
                assertEquals(2, ranged1.getResistBonus());
        }
    }
}


