package edu.ntnu.jonaskos.utils;

import edu.ntnu.jonaskos.Units.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Unit factory test.
 */
class UnitFactoryTest {
    UnitFactory unitFactory = new UnitFactory();

    /**
     * Tests if the unit factory can create a infantry unit.
     */
    @Test
    void createUnitInfantryTest() {
        Unit infantryUnit = unitFactory.createUnit("InfantryUnit", "infantry", 100);

        assertTrue(infantryUnit instanceof InfantryUnit);
    }

    /**
     * Tests if the unit factory can create a cavalry unit.
     */
    @Test
    void createUnitCavalryTest() {
        Unit cavalryUnit = unitFactory.createUnit("CavalryUnit", "cavalry", 100);

        assertTrue(cavalryUnit instanceof CavalryUnit);
    }

    /**
     * Tests if the unit factory can create a commander unit.
     */
    @Test
    void createUnitCommanderTest() {
        Unit commanderUnit = unitFactory.createUnit("CommanderUnit", "commander", 100);

        assertTrue(commanderUnit instanceof CommanderUnit);
    }

    /**
     * Tests if the unit factory can create a ranged unit.
     */
    @Test
    void createUnitRangedTest() {
        Unit rangerUnit = unitFactory.createUnit("RangedUnit", "ranger", 100);

        assertTrue(rangerUnit instanceof RangedUnit);
    }


    /**
     * Tests if the unit factory can create a list of units of the correct size.
     */
    @Test
    void createMultipleUnits() {
        ArrayList<Unit> units = (ArrayList<Unit>) unitFactory.createMultipleUnits("InfantryUnit", "infantry", 100, 5);

        assertEquals(5, units.size());
    }

    /**
     * Tests if the unit factory throws an exception when the unit type is not found.
     */
    @Test
    void createUnitInvalidTest() {
        assertThrows(IllegalArgumentException.class, () -> {
            Unit invalidUnit = unitFactory.createUnit("InvalidUnit", "invalid", 100);
        });
    }
}