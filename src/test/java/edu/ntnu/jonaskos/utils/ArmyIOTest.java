package edu.ntnu.jonaskos.utils;

import edu.ntnu.jonaskos.Sim.Army;
import edu.ntnu.jonaskos.Units.CavalryUnit;
import edu.ntnu.jonaskos.Units.CommanderUnit;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class ArmyIOTest {



    /**
     * Read test.
     * Adds four units to the army and removes one after being saved. then loads army to see if the removed unit is back after army is read from file.
     */
    @Test
    void saveAndReadArmyTest(){
        ArmyIO armyIO = new ArmyIO();
        Army orcs = new Army("orcs");
        orcs.addUnit(new CavalryUnit("cavalry", 100));
        orcs.addUnit(new CavalryUnit("cavalry2", 100));
        orcs.addUnit(new CommanderUnit("commander1", 110));
        CommanderUnit commanderToRemove = new CommanderUnit("commanderRemove", 120);

        try {
            orcs.addUnit(commanderToRemove);
            armyIO.writeArmy(orcs);
            orcs.removeUnit(commanderToRemove);
            orcs = armyIO.loadArmy("orcs");
        } catch (IOException e) {
            e.printStackTrace();
        }

        assertEquals(4,orcs.getAllUnits().size());

    }

    /**
     * Read test.
     * saves army and then loads army to see if the name is the same.
     */


    @Test
    void ReadTestName(){
        ArmyIO armyIO = new ArmyIO();
        Army orcs = new Army("orcs");
        orcs.addUnit(new CavalryUnit("cavalry", 100));

        try {
            armyIO.writeArmy(orcs);
            orcs = armyIO.loadArmy("orcs");
        } catch (IOException e) {
            e.printStackTrace();
        }

        assertEquals("Orcs",orcs.getName());
    }
}