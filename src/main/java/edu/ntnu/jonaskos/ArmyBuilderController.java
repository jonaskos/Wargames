package edu.ntnu.jonaskos;

import edu.ntnu.jonaskos.Sim.Army;
import edu.ntnu.jonaskos.Sim.TerrainModifier;
import edu.ntnu.jonaskos.utils.UnitFactory;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * The class Army builder controller.
 * Controller for the army builder view.
 * makes it possible to create armies and add units to them.
 * take an army in as a parameter and edits the existing army.
 * this means that an existing army is emptied of all units and then the new units are added.
 */
public class ArmyBuilderController {
    Army army = new Army("No name given");
    UnitFactory unitFactory = new UnitFactory();
    private final String[] unitTypes = {"CavalryUnit", "InfantryUnit", "RangedUnit", "CommanderUnit"};

    @FXML
    private TextField amountOfUnit;

    @FXML
    private TextField currentNameText;

    @FXML
    private TextField hpOfUnit;

    @FXML
    private TextField nameOfArmyInput;

    @FXML
    private TextField nameOfUnit;

    @FXML
    private ChoiceBox<String> unitTypeChoise;

    @FXML
    private TextArea unitsInArmyText;

    @FXML
    private Button createArmyButton;


    /**
     * method for setting army to the army that is to be removed/edited
     * current army is emptied of units
     *
     * @param army the army to set
     */
    public void setArmy(Army army){
        this.army = army;
        this.army.getAllUnits().removeAll(this.army.getAllUnits());
        unitTypeChoise.getItems().addAll(unitTypes);
        try {
            System.out.println("Army: " + army);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * applies the name of the army to the army
     *
     * @param actionEvent the action event
     */
    public void applyName(ActionEvent actionEvent) {
        army.setName(nameOfArmyInput.getText());
        currentNameText.setText(army.getName());
    }

    /**
     * adds inputted amount of specified units to the army
     *
     * @param actionEvent the action event
     */
    public void addUnit(ActionEvent actionEvent) {
        try {
            int amount = Integer.parseInt(amountOfUnit.getText());
            String unitType = unitTypeChoise.getValue();
            String name = nameOfUnit.getText();
            int hp = Integer.parseInt(hpOfUnit.getText());
            if(amount > 1){
                army.addAllUnits(new ArrayList<>(unitFactory.createMultipleUnits(unitType, name, hp, amount)));
            } else {
                army.addUnit(unitFactory.createUnit(unitType, name, hp));
            }
            unitsInArmyText.setText(army.toString());
        } catch (Exception e){
            e.printStackTrace();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Could not add units");
            alert.setContentText("please ensure there are no letters in amount of units or unit hp");
            alert.showAndWait();
        }
    }

    /**
     * Closes the build army window
     *
     * @param actionEvent the action event
     */
    public void confirmArmy(ActionEvent actionEvent) {
        Stage stage = (Stage) createArmyButton.getScene().getWindow();
        stage.close();
    }
}
