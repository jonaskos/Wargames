package edu.ntnu.jonaskos;

import edu.ntnu.jonaskos.Sim.Army;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * The type View details controller.
 * Controller for the view details view.
 */
public class ViewDetailsController {

    @FXML
    private TextArea cavalryUnitsDetails;

    @FXML
    private TextArea commanderUnitsDetails;

    @FXML
    private TextArea infantryUnitsDetails;

    @FXML
    private TextArea rangedUnitsDetails;


    /**
     * method for displaying the details of the army
     * displays detailed information about each type of unit in the army.
     *
     * @param army the army to display the details of
     */
    public void displayArmy(Army army){
        try {
            cavalryUnitsDetails.setText(army.getCavalryUnitsString());
            commanderUnitsDetails.setText(army.getCommanderUnitsString());
            infantryUnitsDetails.setText(army.getInfantryUnitsString());
            rangedUnitsDetails.setText(army.getRangedUnitsString());
        } catch (Exception e){
            //todo: real alert
            e.printStackTrace();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error displaying army");
            alert.setHeaderText("Fatal error occurred");
            alert.setContentText("All hope is lost.");
            alert.showAndWait();
        }
    }

}
