package edu.ntnu.jonaskos.Units;

import edu.ntnu.jonaskos.Sim.TerrainModifier;

import java.io.Serializable;

/**
 * The type Cavalry unit.
 */
public class CavalryUnit extends Unit {

    private final static int DEFAULT_ATTACK = 20;
    private final static int DEFAULT_ARMOR = 12;
    private final static int FIRST_ATTACK_BONUS = 6;
    private final static int DEFAULT_ATTACK_BONUS = 2;
    private final static int DEFAULT_RESIST_BONUS = 1;
    private final static int TERRAIN_PLAINS_MODIFIER = 2;
    private int attackCounter = 0;


    /**
     * Instantiates a new Cavalry unit.
     *
     * @param name   the name
     * @param health the health
     * @param attack the attack
     * @param armor  the armor
     */
    public CavalryUnit(String name, int health, int attack, int armor){
        super(name, health, attack, armor);
    }

    /**
     * Instantiates a new Cavalry unit.
     *
     * @param name   the name
     * @param health the health
     */
    public CavalryUnit(String name, int health){
        super(name, health, DEFAULT_ATTACK, DEFAULT_ARMOR);
    }

    /**
     * the attack bonus is higher on the first attack from this unit. the attack bonus is lowered on subsequent attacks.
     * might cause issues if getAttackBonus has to be called for other reasons than being used in the attack method.
     * @return AttackBonus
     */


    public int getAttackBonus(){
        attackCounter++;
        if(TerrainModifier.getTerrain() == 2){

            if (attackCounter == 1){
                return FIRST_ATTACK_BONUS + TERRAIN_PLAINS_MODIFIER;
            } else {
                return DEFAULT_ATTACK_BONUS + TERRAIN_PLAINS_MODIFIER;
            }
        } else {
            if(attackCounter == 1){
                return FIRST_ATTACK_BONUS;
            } else {
                return DEFAULT_ATTACK_BONUS;
            }
        }

    }

    public int getResistBonus(){
        if(TerrainModifier.getTerrain() == 3){
            return 0;
        } else {
            return DEFAULT_RESIST_BONUS;
        }
    }
}
