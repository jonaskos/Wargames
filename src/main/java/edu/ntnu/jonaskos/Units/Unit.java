package edu.ntnu.jonaskos.Units;

/**
 * The type Unit.
 */
public abstract class Unit {
    private final String name;
    private int health;
    private final int attack;
    private final int armor;

    /**
     * Instantiates a new Unit.
     *
     * @param name   the name of the unit
     * @param health the healthpoints of the unit
     * @param attack the attack value of the unit
     * @param armor  the armor value of the unit
     * @throws IllegalArgumentException if no name is given or health value is less than zero
     */
    public Unit(String name, int health, int attack, int armor) throws IllegalArgumentException {
        if(name.isBlank()) throw new IllegalArgumentException("name can not be empty");
        if(health <=0) throw new IllegalArgumentException("health value of unit can not be less or equal to 0");

        this.name = name;
        this.health = health;
        this.attack = attack;
        this.armor = armor;
    }


    /**
     * Gets the name of the unit.
     *
     * @return the name of the unit
     */
    public String getName() {
        return name;
    }

    /**
     * Gets health of the unit.
     *
     * @return the health of the unit
     */
    public int getHealth() {
        return health;
    }

    /**
     * Gets attack value of the unit.
     *
     * @return the attack value of the unit
     */
    public int getAttack() {
        return attack;
    }

    /**
     * Gets armor value of the unit.
     *
     * @return the armor value of the unit
     */
    public int getArmor() {
        return armor;
    }

    /**
     * Set health of the unit.
     *
     * @param health the new health of the unit
     */
    public void setHealth(int health){
        this.health = health;
    }

    /**
     * Gets attack bonus of the unit.
     *
     * @return the attack bonus of the unit
     */
    public abstract int getAttackBonus();

    /**
     * Gets resist bonus of the unit.
     *
     * @return the resist bonus of the unit
     */
    public abstract int getResistBonus();

    /**
     * method for attacking another unit.
     *  calculates the hp of the opponent unit after the attack and set the hp of the opponent unit to the new value.
     * @param opponent the opponent to be attacked
     */
    public void attack(Unit opponent){
        int healthBeforeAttack = opponent.getHealth();
        int healthAfterAttack = opponent.getHealth() - (this.getAttack() + this.getAttackBonus()) + (opponent.getArmor() + opponent.getResistBonus());

        if(healthAfterAttack < healthBeforeAttack) {
            opponent.setHealth(healthAfterAttack);
        }
    }

    @Override
    public String toString() {
        return "Name: " + this.name + " Health: " + this.health + " attackpower: " + this.attack  + " armor: " + this.armor + "\n";
    }
}
