package edu.ntnu.jonaskos.Units;

import edu.ntnu.jonaskos.Sim.TerrainModifier;

public class RangedUnit extends Unit {

    private final static int DEFAULT_ATTACK = 15;
    private final static int DEFAULT_ARMOR = 8;
    private final static int DEFAULT_ATTACK_BONUS = 3;
    private final static int INITIAL_RESIST_BONUS = 6;
    private final static int DEFAULT_RESIST_BONUS = 2;
    private final static int TERRAIN_HILL_MODIFIER = 2;
    private final static int TERRAIN_FOREST_MODIFIER = 2;
    private int attackCounter = 0;

    public RangedUnit(String name, int health, int attack, int armor){
        super(name, health, attack, armor);
    }

    public RangedUnit(String name, int health){
        super(name, health, DEFAULT_ATTACK, DEFAULT_ARMOR);
    }

    public int getAttackBonus(){
        if(TerrainModifier.getTerrain() == 1){
            return DEFAULT_ATTACK_BONUS + TERRAIN_HILL_MODIFIER;
        } else if(TerrainModifier.getTerrain() == 3){
            return DEFAULT_ATTACK_BONUS - TERRAIN_FOREST_MODIFIER;
        }
        else {
            return DEFAULT_ATTACK_BONUS;
        }
    }

    public int getResistBonus(){
        attackCounter++;
        if(attackCounter == 1){
            return INITIAL_RESIST_BONUS;
        } else if(attackCounter == 2){
            return INITIAL_RESIST_BONUS - DEFAULT_RESIST_BONUS;
        }else{
            return DEFAULT_RESIST_BONUS;
        }



    }
}
