package edu.ntnu.jonaskos.Units;

import edu.ntnu.jonaskos.Sim.TerrainModifier;

public class InfantryUnit extends Unit {
    private final static int DEFAULTATTACK = 15, DEFAULTARMOR = 10;
    private final static int DEFAULT_ATTACK_BONUS = 2;
    private final static int DEFAULT_RESIST_BONUS = 1;
    private final static int TERRAIN_FOREST_MODIFIER = 3;


    public InfantryUnit(String name, int health, int attack, int armor){
        super(name, health, attack, armor);
    }

    public InfantryUnit(String name, int health){
        super(name, health, DEFAULTATTACK, DEFAULTARMOR);
    }

    public int getAttackBonus(){
        if(TerrainModifier.getTerrain() == 3){
            return DEFAULT_ATTACK_BONUS + TERRAIN_FOREST_MODIFIER;
    } else {
            return DEFAULT_ATTACK_BONUS;
        }
    }
    public int getResistBonus(){
        if(TerrainModifier.getTerrain() == 3){
            return DEFAULT_RESIST_BONUS + TERRAIN_FOREST_MODIFIER;
        } else {
            return DEFAULT_RESIST_BONUS;
        }
    }
}
