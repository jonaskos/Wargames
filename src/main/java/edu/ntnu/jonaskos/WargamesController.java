package edu.ntnu.jonaskos;

import edu.ntnu.jonaskos.Sim.Army;
import edu.ntnu.jonaskos.Sim.Battle;
import edu.ntnu.jonaskos.Sim.TerrainModifier;
import edu.ntnu.jonaskos.utils.ArmyIO;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

/**
 * The class Wargames controller.
 * Controller class for the main window of the application.
 */
public class WargamesController implements Initializable {

    private Army armyOne;
    private Army armyTwo;
    private Army armyOneCopy;
    private Army armyTwoCopy;
    private final ArmyIO armyIO = new ArmyIO();
    private Stage stage;
    private Scene scene;
    private Parent root;

    @FXML
    private ProgressBar army1Progress;

    @FXML
    private Label army1ProgressLabel;

    @FXML
    private ProgressBar army2Progress;

    @FXML
    private Label army2ProgressLabel;

    @FXML
    private ChoiceBox<String> chooseTerrain;

    @FXML
    private TextField filenameArmy1;

    @FXML
    private TextField filenameArmy2;

    @FXML
    private Label labelArmy1;

    @FXML
    private Label labelArmy2;

    @FXML
    private ImageView terrainVisual;

    @FXML
    private TextField totalCavalryArmy1;

    @FXML
    private TextField totalCavalryArmy2;

    @FXML
    private TextField totalCommandersArmy1;

    @FXML
    private TextField totalCommandersArmy2;

    @FXML
    private TextField totalInfantryArmy1;

    @FXML
    private TextField totalInfantryArmy2;

    @FXML
    private TextField totalRangedArmy1;

    @FXML
    private TextField totalRangedArmy2;

    @FXML
    private TextField totalUnitsArmy1;

    @FXML
    private TextField totalUnitsArmy2;

    @FXML
    private Label winnerLabel;

    @FXML
    private ImageView army1Display;

    @FXML
    private ImageView army2Display;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        chooseTerrain.getItems().addAll(TerrainModifier.getTerrainTypes());
        army1Display.setImage(new Image(String.valueOf(getClass().getResource("data/Images/army1placeholder.jpg"))));
        army2Display.setImage(new Image(String.valueOf(getClass().getResource("data/Images/army2placeholder.jpg"))));
        armyOne = new Army("ArmyOne");
        armyTwo = new Army("ArmyTwo");
        updateArmies();
    }

    /**
     * method for loading specified army as army one
     *
     * @param actionEvent the action event
     */
    public void loadArmy1(ActionEvent actionEvent) {
        try {
            armyOne = armyIO.loadArmy(filenameArmy1.getText());
            updateArmies();
        }catch (IOException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Could not load army");
            alert.setContentText("Please check the filename");
            alert.showAndWait();
        }
    }

    /**
     * method for loading specified army as army two
     *
     * @param actionEvent the action event
     */
    public void loadArmy2(ActionEvent actionEvent) {
        try {
            armyTwo = armyIO.loadArmy(filenameArmy2.getText());
            updateArmies();
        } catch (IOException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Could not load army");
            alert.setContentText("Please check the filename");
            alert.showAndWait();
        }
    }

    /**
     * method for saving army one to specified filename
     *
     * @param actionEvent the action event
     */
    public void saveArmy1(ActionEvent actionEvent) {
        try {
            armyOne.setName(filenameArmy1.getText());
            String saveLocation = armyIO.writeArmy(armyOne);
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Army saved");
            alert.setHeaderText("Army has been saved");
            alert.setContentText("save location: " + saveLocation);
            alert.showAndWait();
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Could not save army");
            alert.setContentText("Please check the filename");
            alert.showAndWait();
        }
    }

    /**
     * method for saving army two to specified filename
     *
     * @param actionEvent the action event
     */
    public void saveArmy2(ActionEvent actionEvent) {
        try {
            armyTwo.setName(filenameArmy2.getText());
            String saveLocation = armyIO.writeArmy(armyTwo);
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Army saved");
            alert.setHeaderText("Army has been saved");
            alert.setContentText("save location: " + saveLocation);
            alert.showAndWait();
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Could not save army");
            alert.setContentText("Please check the filename");
            alert.showAndWait();
        }
    }

    /**
     * method for simulating battle between army one and army two
     * a copy of the army before the simulation is run to be used when the user wants to reset the battle
     * timeout beetween steps of the simulation is calculated based on total amount of health of the armies.
     * Progressbar for each army is updated for every "round" of the simulation
     * @param actionEvent the action event
     */
    public void simulateBattle(ActionEvent actionEvent) {

        try {
            if(!armyOne.hasUnits() || !armyTwo.hasUnits()){
                throw new IllegalArgumentException();
            }
            this.armyOneCopy = new Army(armyOne);
            this.armyTwoCopy = new Army(armyTwo);

            long simulationTime = (long) (100000/(armyOne.getTotalhp() + armyTwo.getTotalhp()));

            Battle battle = new Battle(armyOne, armyTwo);
            winnerLabel.setText("Simulating...");
            Task<Void> task = new Task<>(){
                @Override public Void call() {
                    while (battle.getWinner() == null){
                        battle.simulateAttack();
                        army1Progress.setProgress(armyOne.getTotalhp()/armyOneCopy.getTotalhp());
                        army2Progress.setProgress(armyTwo.getTotalhp()/armyTwoCopy.getTotalhp());
                        try {
                            TimeUnit.MILLISECONDS.sleep(simulationTime);
                        } catch (InterruptedException e) {
                            Alert alert = new Alert(Alert.AlertType.ERROR);
                            alert.setTitle("Error");
                            alert.setHeaderText("Simulation failed");
                            alert.setContentText("Thread Interrupted. Try restarting simulation.");
                            alert.showAndWait();
                        }
                    }

                    return null;
                }
            };
            task.setOnSucceeded(s -> {
                winnerLabel.setText(battle.getWinner().getName() + " Wins!");
                updateArmies();
            });
            new Thread(task).start();

        } catch (Exception e){
            if(e instanceof NullPointerException){
                e.printStackTrace();
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("Simulation failed");
                alert.setContentText("You need to apply terrain before simulating battle and none of the armies can be empty");
                alert.showAndWait();
            }else if(e instanceof IllegalArgumentException){
                e.printStackTrace();
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("Simulation failed");
                alert.setContentText("Creating backup of army has failed.");
                alert.showAndWait();
            }
            else {
                e.printStackTrace();
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("Simulation failed");
                alert.setContentText("Please ensure two armies are ready or that simulation has been reset.");
                alert.showAndWait();
            }

        }
    }

    /**
     * method for setting the information displays of the armies so that they can be seen after simulating or loading/creating an army.
     * the progressbars will be set so that the "strength" of the armies are relative to each other.
     */
    public void updateArmies(){
            if(armyOne.getTotalhp() == armyTwo.getTotalhp()){
                army1Progress.setProgress(1);
                army2Progress.setProgress(1);
            }
            else if(armyOne.getTotalhp() > armyTwo.getTotalhp()){
                army1Progress.setProgress(1);
                army2Progress.setProgress(armyTwo.getTotalhp()/armyOne.getTotalhp());
            } else {
                army2Progress.setProgress(1);
                army1Progress.setProgress(armyOne.getTotalhp()/armyTwo.getTotalhp());
            }

            labelArmy1.setText(armyOne.getName());
            totalUnitsArmy1.setText(String.valueOf(armyOne.getAllUnits().size()));
            totalInfantryArmy1.setText(String.valueOf(armyOne.getInfantryUnits().size()));
            totalRangedArmy1.setText(String.valueOf(armyOne.getRangedUnits().size()));
            totalCavalryArmy1.setText(String.valueOf(armyOne.getCavalryUnits().size()));
            totalCommandersArmy1.setText(String.valueOf(armyOne.getCommanderUnits().size()));
            army1ProgressLabel.setText(armyOne.getName() + " Strength");

            labelArmy2.setText(armyTwo.getName());
            totalUnitsArmy2.setText(String.valueOf(armyTwo.getAllUnits().size()));
            totalInfantryArmy2.setText(String.valueOf(armyTwo.getInfantryUnits().size()));
            totalRangedArmy2.setText(String.valueOf(armyTwo.getRangedUnits().size()));
            totalCavalryArmy2.setText(String.valueOf(armyTwo.getCavalryUnits().size()));
            totalCommandersArmy2.setText(String.valueOf(armyTwo.getCommanderUnits().size()));
            army2ProgressLabel.setText(armyTwo.getName() + " Strength");


    }

    //todo: better way of doing this maybe

    /**
     * applies the selected the terrain
     *
     * @param actionEvent the action event
     */
    public void applyTerrain(ActionEvent actionEvent) {
        try {
            if (chooseTerrain.getValue().equals("PLAINS")) {
                terrainVisual.setImage(new Image(String.valueOf(getClass().getResource("data/Images/Plains.jpg"))));
            } else if (chooseTerrain.getValue().equals("FOREST")) {
                terrainVisual.setImage(new Image(String.valueOf(getClass().getResource("data/Images/Forest.jpg"))));
            } else if (chooseTerrain.getValue().equals("HILL")) {
                terrainVisual.setImage(new Image(String.valueOf(getClass().getResource("data/Images/Hill.jpg"))));
            }
            TerrainModifier.setTerrain(chooseTerrain.getValue());
        } catch (NullPointerException e){
            e.printStackTrace();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Could not apply terrain");
            alert.setContentText("Please select a terrain to apply.");
            alert.showAndWait();
        }
    }

    /**
     * method for resetting the armies to their original state before the simulated battle
     * Will alert the user if the user try to reset the simulation multiple times.
     * @param actionEvent the action event
     */
    public void resetBattle(ActionEvent actionEvent) {
        if(armyOne.hasUnits() && armyTwo.hasUnits()){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Simulation already reset");
            alert.setContentText("You have already reset the simulation or the simulation is in progress");
            alert.showAndWait();
        } else {
            this.armyOne = new Army(armyOneCopy);
            this.armyTwo = new Army(armyTwoCopy);
            updateArmies();
            winnerLabel.setText("Winner Display");
        }
    }

    /**
     * method for showing the details of the selected army in a new window
     *
     * @param army the army to be shown
     */
    public void viewDetails(Army army) {

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("ViewDetailsFXML.fxml"));
        try {
            root = fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Could not open detailed view");
            alert.setContentText("Loading of fxml failed");
            alert.showAndWait();
        }

        ViewDetailsController viewDetailsController = fxmlLoader.getController();
        viewDetailsController.displayArmy(army);

        stage = new Stage();
        stage.setTitle("Wargames: Detailed View");
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * method for showing the details of army one
     *
     * @param actionEvent the action event
     */
    public void openDetailsArmy1(ActionEvent actionEvent) {
        viewDetails(armyOne);
    }

    /**
     * method for showing the details of army two
     *
     * @param actionEvent the action event
     */
    public void openDetailsArmy2(ActionEvent actionEvent) {
        viewDetails(armyTwo);
    }


    /**
     * method for opening the army builder window
     *
     * @param army the army to be edited
     */
    public void buildArmySetup(Army army) {

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("ArmyBuilderFXML.fxml"));
        try {
            root = fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Build army window could not be opened");
            alert.setContentText("Loading of fxml failed");
            alert.showAndWait();
        }

        ArmyBuilderController armyBuilderController = fxmlLoader.getController();
        armyBuilderController.setArmy(army);

        stage = new Stage();
        scene = new Scene(root);
        stage.setTitle("Wargames: ArmyBuilder");
        stage.setScene(scene);
        stage.showAndWait();
        updateArmies();
    }


    /**
     * method for opening the army builder window for army one
     *
     * @param actionEvent the action event
     */
    public void buildArmy1(ActionEvent actionEvent) {
        buildArmySetup(armyOne);
    }

    /**
     * method for opening the army builder window for army two
     *
     * @param actionEvent the action event
     */
    public void buildArmy2(ActionEvent actionEvent) {
        buildArmySetup(armyTwo);
    }
}
