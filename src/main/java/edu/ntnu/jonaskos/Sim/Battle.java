package edu.ntnu.jonaskos.Sim;


import edu.ntnu.jonaskos.Units.Unit;

import java.util.Random;

/**
 * The battle class.
 * responsible for the battle between two armies
 */

public class Battle {

    private final Army armyOne;
    private final Army armyTwo;
    private Army winner;

    /**
     * Instantiates a new Battle.
     *
     * @param armyOne army number one
     * @param armyTwo army number two
     */
    public Battle(Army armyOne, Army armyTwo) {
        this.armyOne = armyOne;
        this.armyTwo = armyTwo;
    }

    /**
     * Simulate battles
     * one unit gets picked out at random from each army as either the attacking unit
     * or the defending unit
     * this loops until one of the armies is empty
     * the army with units left gets returned
     * @return the winning army
     */
    public Army simulate(){
        while (armyOne.hasUnits() && armyTwo.hasUnits()) {
            simulateAttack();
        }
        if (armyOne.hasUnits()) {
            return armyOne;
        } else return armyTwo;
    }


    /**
     * Simulate attack method.
     * picks a unit from each army at random and attacks the other
     * if the defending unit is killed, it is removed from the army
     * if one of the armies is empty, the other one is set as the winner
     */
    public void simulateAttack(){
        int attackingArmy = new Random().nextInt(0, 2);
        Unit unitAttacker;
        Unit unitDefender;
        if (attackingArmy == 0) {
            unitAttacker = armyOne.getRandomUnit();
            unitDefender = armyTwo.getRandomUnit();
            unitAttacker.attack(unitDefender);

            if (unitDefender.getHealth() <= 0) {
                armyTwo.removeUnit(unitDefender);
            }
        } else {

            unitAttacker = armyTwo.getRandomUnit();
            unitDefender = armyOne.getRandomUnit();
            unitAttacker.attack(unitDefender);

            if (unitDefender.getHealth() <= 0) {
                armyOne.removeUnit(unitDefender);
            }


        }
        if(!armyOne.hasUnits()){
            winner = armyTwo;
        } else if(!armyTwo.hasUnits()){
            winner = armyOne;
        }

    }

    /**
     * gets winner army
     *
     * @return army set as the winner.
     */
    public Army getWinner(){
        return winner;
    }
}
