package edu.ntnu.jonaskos.Sim;

import edu.ntnu.jonaskos.Units.*;
import edu.ntnu.jonaskos.utils.ArmyIO;
import edu.ntnu.jonaskos.utils.UnitFactory;

import java.util.*;
import java.util.stream.Collectors;

/**
 * The type Army.
 * stores units and its name
 */
public class Army {

    private String name;
    private final ArrayList<Unit> units;

    /**
     * Instantiates a new Army.
     *
     * @param name the name of the army
     */
    public Army(String name){
        this.name = name;
        this.units = new ArrayList<>();
    }

    /**
     * copy constructor for an army
     * Instantiates a new Army.
     * This method is used to copy another army.
     * @param army the army
     */
    public Army(Army army) throws IllegalArgumentException {
        this.name = army.getName();
        this.units = new ArrayList<>();
        for (Unit unit : army.getAllUnits()) {
            UnitFactory unitFactory = new UnitFactory();
            this.units.add(unitFactory.createUnit(unit.getClass().getSimpleName(), unit.getName(), unit.getHealth()));
        }

    }

    /**
     * Instantiates a new Army.
     * This method is used to create an army using a list of units.
     * @param name  the name of the army
     * @param units the units of the army
     */
    public Army(String name, ArrayList<Unit> units){
        this.name = name;
        this.units = units;
    }

    /**
     * Gets name.
     *
     * @return the name of the army
     */
    public String getName() {
        return name;
    }


    /**
     * Set name.
     *
     * @param name the new name to set for the army
     */
    public void setName(String name){
        this.name = name;
    }

    /**
     * Adds a unit to the army.
     *
     * @param unit the unit to be added to the army
     */
    public void addUnit(Unit unit){
        units.add(unit);
    }

    /**
     * Adds all units from an input arraylist.
     *
     * @param units the units to be added to the army
     */
    public void addAllUnits(ArrayList<Unit> units){
        for(Unit unit : units){
            addUnit(unit);
        }
    }

    /**
     * Remove unit from the army.
     *
     * @param unit the unit to be removed from the army.
     */
    public void removeUnit(Unit unit){
        units.remove(unit);
    }

    /**
     * Has units boolean.
     *
     * @return true if the army has units or false when the army has no more units
     */
    public boolean hasUnits(){
        return !units.isEmpty();
    }

    /**
     * Get all units in the army
     *
     * @return arraylist of all units currently in the army
     */
    public ArrayList<Unit> getAllUnits(){
        return units;
    }

    /**
     * Get random unit.
     *
     * @return A random unit from the army.
     */
    public Unit getRandomUnit(){
        if(hasUnits()) {
            return units.get(new Random().nextInt(units.size()));
        }
        return null;
    }

    /**
     * Get infantry units list.
     *
     * @return the list of all infantry units in the army.
     */
    public List<Unit> getInfantryUnits(){
        return units.stream().filter(u -> u instanceof InfantryUnit).collect(Collectors.toList());
    }


    /**
     * Get cavalry units list.
     *
     * @return the list of all cavalry units in the army.
     */
    public List<Unit> getCavalryUnits(){
        return units.stream().filter(u -> u instanceof CavalryUnit && !(u instanceof CommanderUnit)).collect(Collectors.toList());
    }

    /**
     * Get ranged units list.
     *
     * @return the list of all ranged units in the army.
     */
    public List<Unit> getRangedUnits(){
        return units.stream().filter(u -> u instanceof RangedUnit).collect(Collectors.toList());
    }

    /**
     * Get commander units as a list.
     *
     * @return the list of all commander units in the army
     */
    public List<Unit> getCommanderUnits(){
        return units.stream().filter(u -> u instanceof CommanderUnit).collect(Collectors.toList());
    }
    /**
     * Get Infantry units as a list.
     *
     * @return the list of all infantry units in the army
     */
    public String getInfantryUnitsString(){
        return getInfantryUnits().toString().replace("[","").replace("]","").replace(", ","");
    }
    /**
     * Get Cavalry units as a list.
     *
     * @return the list of all cavalry units in the army
     */
    public String getCavalryUnitsString(){
        return getCavalryUnits().toString().replace("[","").replace("]","").replace(", ","");
    }
    /**
     * Get ranged units as a list.
     *
     * @return the list of all ranged units in the army
     */
    public String getRangedUnitsString(){
        return getRangedUnits().toString().replace("[","").replace("]","").replace(", ","");
    }
    /**
     * Get commander units as a list.
     *
     * @return the list of all commander units in the army
     */
    public String getCommanderUnitsString(){
        return getCommanderUnits().toString().replace("[","").replace("]","").replace(", ","");
    }

    /**
     * Gets total hp of an army.
     * adds all units hp together.
     * @return total hp of the army
     */
    public double getTotalhp(){
        return units.stream().mapToInt(Unit::getHealth).sum();
    }


    @Override
    public String toString() {
        return name + ":\n" + units.toString().replace(",","").replace("[","").replace("]","");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Army army = (Army) o;
        return getName().equals(army.getName()) && units.equals(army.units);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), units);
    }
}
