package edu.ntnu.jonaskos.Sim;

import java.util.HashMap;
import java.util.Locale;

/**
 * The type Terrain modifier.
 * This class is used to modify the terrain.
 * attackbonus and defensebonus are calculated using getTerrain method.
 */
public class TerrainModifier {

    /**
     * The Terrain modifier.
     */
    static final TerrainModifier terrainModifier = new TerrainModifier();
    private String terrain;
    private final HashMap<String, Integer> terrainID;

    private TerrainModifier(){
        this.terrainID = new HashMap<>();
        terrainID.put("HILL", 1);
        terrainID.put("PLAINS", 2);
        terrainID.put("FOREST", 3);
    }

    /**
     * Get terrain int.
     * returns the terrain as an int for use in
     * the different units for calculating attackbonus and resistbonus
     *
     * @return the int
     */
    public static int getTerrain(){
        return terrainModifier.terrainID.get(terrainModifier.terrain.toUpperCase(Locale.ROOT));

    }

    /**
     * Set terrain.
     * sets the terrain to the given string
     * @param terrain the terrain
     */
    public static void setTerrain(String terrain){
        terrainModifier.terrain = terrain;
    }

    /**
     * Get terrain types
     * returns a string array of the terrain types
     *
     * @return the string []
     */
    public static String[] getTerrainTypes(){
        return terrainModifier.terrainID.keySet().toArray(new String[0]);
    }
}
