package edu.ntnu.jonaskos.utils;

import edu.ntnu.jonaskos.Units.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The type Unit factory.
 */
public class UnitFactory {


    /**
     * method for creating a single unit
     *
     * @param unitToCreate the unit to create
     * @param name         the name of the unit
     * @param healthPoints the health points of the unit
     * @return the unit that was created
     * @throws IllegalArgumentException throws an exception if the unit is not a valid unit
     */
    public Unit createUnit(String unitToCreate, String name, int healthPoints) throws IllegalArgumentException {

        return switch (unitToCreate) {
            case "CavalryUnit" -> new CavalryUnit(name, healthPoints);
            case "CommanderUnit" -> new CommanderUnit(name, healthPoints);
            case "InfantryUnit" -> new InfantryUnit(name, healthPoints);
            case "RangedUnit" -> new RangedUnit(name, healthPoints);
            default -> throw new IllegalArgumentException("Type of unit does not exist");
        };
    }

    /**
     * method for creating multiple units of a given type
     * all units will have the same name and health points
     *
     * @param unitToCreate  the type of unit to create
     * @param name          the name of the units
     * @param healthPoints  the health points of the units
     * @param numberOfUnits the number of units to create
     * @return the list of units that was created
     */
    public List<Unit> createMultipleUnits(String unitToCreate, String name, int healthPoints, int numberOfUnits){

        List<Unit> newUnits = new ArrayList<>();

        switch (unitToCreate) {
            case "CavalryUnit":

                for(int i = 0; i < numberOfUnits; i++){
                    newUnits.add(new CavalryUnit(name, healthPoints));
                }

                break;
            case "CommanderUnit":
                for(int i = 0; i < numberOfUnits; i++){
                    newUnits.add(new CommanderUnit(name, healthPoints));
                }

                break;
            case "InfantryUnit":
                for(int i = 0; i < numberOfUnits; i++){
                    newUnits.add(new InfantryUnit(name, healthPoints));
                }
                break;
            case "RangedUnit":
                for(int i = 0; i < numberOfUnits; i++){
                    newUnits.add(new RangedUnit(name, healthPoints));
                }
                break;
            default:
                throw new IllegalArgumentException("Type of unit does not exist");
        }
        return newUnits;
    }

}
