package edu.ntnu.jonaskos.utils;

import edu.ntnu.jonaskos.Sim.Army;
import edu.ntnu.jonaskos.Units.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

/**
 * The type Army io.
 */
public class ArmyIO implements Serializable {

    /**
     * The Unit factory.
     */
    UnitFactory unitFactory = new UnitFactory();

    /**
     * method for writing an army to a file
     *
     * @param armyToSave the army to save
     * @return the absolute path of the saved file.
     * @throws IOException throws an exception if the file cannot be written
     */
    public String writeArmy(Army armyToSave) throws IOException {


            File saveFile = new File(armyToSave.getName() + ".csv");
            FileWriter fileWriter = new FileWriter(saveFile, false);
            fileWriter.write(armyToSave.getName().substring(0,1).toUpperCase(Locale.ROOT) + armyToSave.getName().substring(1) + "\n");

            for (Unit unit : armyToSave.getAllUnits()) {
                fileWriter.write(unit.getClass().getSimpleName() +"," + unit.getName() + "," + unit.getHealth() + "\n");
            }
            fileWriter.close();

            return saveFile.getAbsolutePath();
    }


    /**
     * method for reading an army from a file
     *
     * @param name the name of the army that should be loaded
     * @return the army that was read from the file
     * @throws IOException throws an exception if the file cannot be read
     */
    public Army loadArmy(String name) throws IOException {
        ArrayList<String> readAsString = new ArrayList<>();
        File armyFile = new File(name + ".csv");

        Scanner reader = new Scanner(armyFile);

        while(reader.hasNextLine()){
            readAsString.add(reader.nextLine());
        }
        reader.close();


        Army newArmy = new Army(readAsString.get(0));

        for(int i = 1; i < readAsString.size(); i++){
            String[] unitsInArray = readAsString.get(i).split(",");

            newArmy.addUnit(unitFactory.createUnit(unitsInArray[0], unitsInArray[1], Integer.parseInt(unitsInArray[2])));
        }
    return newArmy;
    }
}
