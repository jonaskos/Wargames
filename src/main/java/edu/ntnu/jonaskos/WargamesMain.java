package edu.ntnu.jonaskos;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * The Class Wargames main.
 * Entry point for the application.
 */
public class WargamesMain  extends Application {



        @Override
        public void start(Stage primaryStage) throws Exception {


            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("WarGameFXML.fxml"));
            Parent content = loader.load();

            Scene scene = new Scene(content);

            primaryStage.setResizable(false);
            primaryStage.setTitle("Wargames: Battle simulator 3000");
            primaryStage.setScene(scene);
            primaryStage.show();
        }

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
